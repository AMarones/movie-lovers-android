package com.amarones.movielovers.view;

import com.amarones.movielovers.entities.Content;

import java.util.List;

public interface MovieListView {
    void showLoading();
    void showMovieList(List<Content> contentList);
    void showInternetError();
    void presentMovieDetailScreen(Content content);
}
