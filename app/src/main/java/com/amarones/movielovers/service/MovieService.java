package com.amarones.movielovers.service;

import com.amarones.movielovers.entities.Content;
import com.amarones.movielovers.entities.MovieSchedule;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieService {

    @GET("/iphone/ws/IngressoService.svc/rest/Conteudo?SecaoSite=1&Item=7&idCidade=00000002&idPais=1&Pai=S&idItemTemplate=135")
    Call<MovieSchedule> getMovieSchedule();
}
