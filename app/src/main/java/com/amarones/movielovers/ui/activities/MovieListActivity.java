package com.amarones.movielovers.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.amarones.movielovers.R;
import com.amarones.movielovers.entities.Content;
import com.amarones.movielovers.presenter.MovieListPresenter;
import com.amarones.movielovers.ui.adapters.MovieAdapter;
import com.amarones.movielovers.view.MovieListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public class MovieListActivity extends BaseActivity implements MovieListView {

    @Bind(R.id.recyclerview)
    public RecyclerView mRecyclerView;

    @Bind(R.id.loading_content)
    public View mLoadingView;

    @Bind(R.id.error_view)
    public View mErrorView;

    @Inject
    MovieListPresenter mMovieListPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_movie_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeInjector();

        initializeView();
    }

    private void initializeInjector() {
        getApplicationComponent().inject(this);
    }

    private void initializeView() {
        mMovieListPresenter.attachView(this);
    }

    @Override
    public void presentMovieDetailScreen(Content content) {
        Intent intent = new Intent(this, MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.EXTRA_MOVIE, content);
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMovieList(List<Content> contentList) {
        mLoadingView.setVisibility(View.GONE);

        MovieAdapter adapter = new MovieAdapter(MovieListActivity.this, contentList);
        adapter.setOnMovieClicked(mMovieListPresenter::onMovieClicked);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(MovieListActivity.this, 2));
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showInternetError() {
        mLoadingView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mErrorView.setVisibility(View.VISIBLE);
    }
}
