package com.amarones.movielovers.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.amarones.movielovers.R;
import com.amarones.movielovers.entities.Content;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "selected_movie";

    @Bind(R.id.toolbar)
    public Toolbar mTtoolbar;

    @Bind(R.id.collapsing_toolbar)
    public CollapsingToolbarLayout mCollapsingToolbar;

    @Bind(R.id.image_main)
    public ImageView mImageHeader;

    @Bind(R.id.text_movie_title)
    public TextView mTextMovieTitle;

    @Bind(R.id.text_movie_genre)
    public TextView mTextMovieGenre;

    @Bind(R.id.text_movie_plot)
    public TextView mTextMoviePlot;

    private Content mMovieContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        mMovieContent = (Content) intent.getSerializableExtra(EXTRA_MOVIE);

        setupToolbar();

        setupViewContent();
    }

    private void setupViewContent() {
        Picasso.with(this).load(mMovieContent.horizontalImageURL).into(mImageHeader);

        mTextMovieTitle.setText(mMovieContent.description);
        mTextMovieGenre.setText(String.format("%s | %s", mMovieContent.genre, mMovieContent.rating));
        mTextMoviePlot.setText(mMovieContent.plot);
    }

    private void setupToolbar() {
        setSupportActionBar(mTtoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCollapsingToolbar.setTitle(" ");
    }
}
