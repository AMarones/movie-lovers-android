package com.amarones.movielovers.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amarones.movielovers.R;
import com.amarones.movielovers.entities.Content;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.functions.Action1;

public class MovieAdapter extends RecyclerView.Adapter <MovieAdapter.MovieViewHolder> {

    private final LayoutInflater layoutInflater;
    private Context mContext;
    private List<Content> mContentList;
    private Action1<Content> mOnMovieClicked;

    public MovieAdapter(Context context, List<Content> contentList) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.mContentList = contentList;
    }

    @Override
    public MovieAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.adapter_movie, parent, false);
        return new MovieViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        Content movieContent = mContentList.get(position);
        holder.bind(movieContent);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return mContentList.size();
    }

    public void setOnMovieClicked(Action1<Content> onMovieClicked) {
        this.mOnMovieClicked = onMovieClicked;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image_poster)
        ImageView imageViewPoster;

        @Bind(R.id.text_movie_name)
        TextView textMovieName;

        Context mContext;

        public MovieViewHolder(View itemView, Context mContext) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mContext = mContext;
        }

        public void bind(Content movieContent) {
            textMovieName.setText(movieContent.description);

            Picasso.with(mContext).load(movieContent.mainImageURL).into(imageViewPoster);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnMovieClicked != null) {
                        mOnMovieClicked.call(movieContent);
                    }
                }
            });
        }
    }
}
