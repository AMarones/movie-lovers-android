package com.amarones.movielovers.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.amarones.movielovers.MovieLoverApplication;
import com.amarones.movielovers.di.ApplicationComponent;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {
    protected abstract int getLayoutResId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResId());

        ButterKnife.bind(this);

        this.getApplicationComponent().inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return ((MovieLoverApplication) getApplication()).getApplicationComponent();
    }
}
