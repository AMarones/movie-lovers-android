package com.amarones.movielovers;

import android.app.Application;

import com.amarones.movielovers.di.ApplicationComponent;
import com.amarones.movielovers.di.DaggerApplicationComponent;
import com.amarones.movielovers.di.modules.ApplicationModule;

public class MovieLoverApplication extends Application {
    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        this.mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
