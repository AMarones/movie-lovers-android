package com.amarones.movielovers.di.modules;

import android.content.Context;

import com.amarones.movielovers.MovieLoverApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final MovieLoverApplication application;

    public ApplicationModule(MovieLoverApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

}
