package com.amarones.movielovers.di;

import com.amarones.movielovers.di.modules.ApplicationModule;
import com.amarones.movielovers.di.modules.PresenterModule;
import com.amarones.movielovers.ui.activities.BaseActivity;
import com.amarones.movielovers.ui.activities.MovieListActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class, PresenterModule.class
})
public interface ApplicationComponent {
    void inject(BaseActivity baseActivity);
    void inject(MovieListActivity movieListActivity);
}
