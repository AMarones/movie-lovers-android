package com.amarones.movielovers.di.modules;

import com.amarones.movielovers.presenter.MovieListPresenter;
import com.amarones.movielovers.presenter.MovieListPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {
    @Provides
    @Singleton
    MovieListPresenter providesMovieListPresenter(MovieListPresenterImpl registerNewProductPresenter) {
        return registerNewProductPresenter;
    }
}
