package com.amarones.movielovers.entities;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "Conteudo")
public class Content implements Serializable {

    @Attribute(name = "Valor")
    public String value;

    @Attribute(name = "ValorPai")
    public String value2;

    @Attribute(name = "Posicao")
    public String position;

    @Attribute(name = "TipoEvento")
    public String eventType;

    @Attribute(name = "QtdLocais")
    public String qtd;

    @Attribute(name = "MediaAvaliacao")
    public String stars;

    @Attribute(name = "QtdVotos")
    public String votes;

    @Attribute(name = "Descricao")
    public String description;

    @Attribute(name = "Genero")
    public String genre;

    @Attribute(name = "Classificacao")
    public String rating;

    @Attribute(name = "ConteudoSinopse")
    public String plot;

    @Attribute(name = "FiguraDestaque")
    public String mainImageURL;

    @Attribute(name = "FiguraHorizontal")
    public String horizontalImageURL;

    @Attribute(name = "Prioridade")
    public String priority;

    public Content() {}
}
