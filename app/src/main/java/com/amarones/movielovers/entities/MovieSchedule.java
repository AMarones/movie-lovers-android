package com.amarones.movielovers.entities;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ProgramacaoResponse")
public class MovieSchedule {

    @Path("ProgramacaoResult")
    @ElementList(inline = true)
    public List<Content> contentList;

    public MovieSchedule() {}
}
