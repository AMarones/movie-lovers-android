package com.amarones.movielovers.presenter;

import android.support.annotation.Nullable;
import android.util.Log;

import com.amarones.movielovers.entities.Content;
import com.amarones.movielovers.entities.MovieSchedule;
import com.amarones.movielovers.service.MovieService;
import com.amarones.movielovers.view.MovieListView;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class MovieListPresenterImpl implements MovieListPresenter {

    @Nullable
    private MovieListView mMovieListView;

    @Inject
    public MovieListPresenterImpl() {
    }

    @Override
    public void attachView(MovieListView movieListView) {
        mMovieListView = movieListView;
        loadMovieList();
    }

    @Override
    public void loadMovieList() {
        if (mMovieListView != null) {
            mMovieListView.showLoading();
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.ingresso.com")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        MovieService apiService = retrofit.create(MovieService.class);

        Call<MovieSchedule> callMovie = apiService.getMovieSchedule();
        callMovie.enqueue(new Callback<MovieSchedule>() {
            @Override
            public void onResponse(Call<MovieSchedule> call, Response<MovieSchedule> response) {
                MovieSchedule movieSchedule = response.body();

                if (mMovieListView != null) {
                    mMovieListView.showMovieList(movieSchedule.contentList);
                }
            }

            @Override
            public void onFailure(Call<MovieSchedule> call, Throwable t) {
                Log.d("error >>> ", t.getLocalizedMessage());

                if (mMovieListView != null) {
                    mMovieListView.showInternetError();
                }
            }
        });
    }

    @Override
    public void onMovieClicked(Content movieCcontent) {
        if (mMovieListView != null) {
            mMovieListView.presentMovieDetailScreen(movieCcontent);
        }
    }
}
