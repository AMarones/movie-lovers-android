package com.amarones.movielovers.presenter;

import com.amarones.movielovers.entities.Content;
import com.amarones.movielovers.view.MovieListView;

public interface MovieListPresenter {
    void attachView(MovieListView movieListView);
    void loadMovieList();
    void onMovieClicked(Content movieCcontent);
}
